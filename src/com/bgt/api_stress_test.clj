(ns com.bgt.api-stress-test
  (:require [com.bgt.exec.caller :as caller])
  (:gen-class))

(defn time-apis
  "Callable entry point to the application."
  [n grouped?]
  (caller/print-result n grouped?))

;; command line clojure -X:uberjar :jar api-stress-test.jar
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (time-apis (Integer/parseInt (first args)) true)
  (System/exit 1))
