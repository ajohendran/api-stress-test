(ns com.bgt.exec.caller
  (:require [hato.client :as hato]
            [cheshire.core :as c]
            [clojure.pprint :as pp]))


(def client (hato/build-http-client {:connect-timeout 10000
                                     :redirect-policy :always}))

(def base-url "http://localhost:5000/v2")

(def states ["AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"])

(defn rand-state [] (nth states (rand 49)))

(defn get-locations [] (take (inc (rand-int 5)) (repeatedly rand-state)))

(defn trending-fn []
  (let [dt (.atDay (java.time.Year/of 2020)
                   (inc (rand-int 365)))
        plusMonth (.plusMonths dt 1)
        plusMonthPlusOne (.plusDays plusMonth 1)
        plusMonthPlusOnePlusMonth (.plusMonths plusMonthPlusOne 1)]
    {:maximumRows 10,
     :dates {:dateFrom (str plusMonthPlusOne),
             :dateTo (str plusMonthPlusOnePlusMonth)},
     :fromdates {:dateFrom (str dt),
                 :dateTo (str plusMonth)},
     :location {:countryCode "US",
                :level 5,
                :locations (get-locations)}}))

(defn available-jobs-count-fn []
  (let [dt (.atDay (java.time.Year/of 2020)
                   (inc (rand-int 365)))
        plusMonth (.plusMonths dt 1)
        min-salary (+ 10000 (rand-int 60000))]
    {:targetDates {:dateFrom (str dt),
		   :dateTo (str plusMonth)},
     :location {:countryCode "US",
	        :level 5,	
	        :locations (get-locations)},
     :mainCategory "Occupation Family",
     :nestedCategory "Industry",
     :jobZoneCodes [1, 2],
     :minSalary min-salary,
     :maxSalary (+ min-salary 30000)}))


(def end-points {:trending-employers {:url "/employer/trendingemployers"
                                      :body-fn trending-fn}
                 :trending-occupations {:url "/occupation/trendingoccupations"
                                        :body-fn trending-fn}
                 :trending-industries {:url "/industry/trendingindustries"
                                       :body-fn trending-fn}
                 :available-job-counts-total {:url "/posting/AvailableJobCountsTotalRecords"
                                              :body-fn available-jobs-count-fn}})



(defn call-api [id]
  (let [end-point (end-points id)
        body ((:body-fn end-point))]
      (println (str "\n\ncalling " id " at " (java.time.LocalDateTime/now) " with body: " body))
      (hato/post (str base-url (:url end-point))
                 {:body (c/generate-string body)
                  :content-type :json
                  :async? false})))


(defn run-test-old [n]
  (pmap call-api (mapcat (partial repeat n) (keys end-points))))

(defn run-test-old-2 [n]
  (pmap call-api (apply concat (repeat n (keys end-points)))))

(defn run-test [n grouped?]
  (let [lst (if grouped?
              (mapcat (partial repeat n) (keys end-points))
              (apply concat (repeat n (keys end-points))))]
    (mapv (fn [id] (future (call-api id))) lst)))

(defn print-result [n grouped?]
  (time
   (let [res (map #(select-keys @% [:uri :request-time]) (run-test n grouped?))]
     (pp/pprint res))))

